﻿using System;
using ENet;
using Common.Classes;
using Common;
using System.IO;
using CsvHelper;
using System.Globalization;

namespace EntryServer
{
	class Program
	{
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		// used during tests in assumption that players do not exit game
		static uint clientNumber = 0;
		static string csvPath;
		static bool exportFirstLine = true;

		static void RedirectClient(Peer peer)
        {
			EntryServerRedirectData serverResponse = new EntryServerRedirectData() { ClientID = clientNumber, Address = Constants.SERVER_HOST, Port = Convert.ToUInt16(Constants.SERVER_PORT_START + (clientNumber % Constants.NUMBER_SERVERS)) }; // port 5001 - 5004
			ServerMessage serverMessage = new ServerMessage() { MessageType = MessageType.EntryServerRedirect, Data = serverResponse };

			Packet packet = default(Packet);
            byte[] data = serverMessage.Serialize();

			packet.Create(data, PacketFlags.Reliable);
            peer.Send(0, ref packet);

			Logger.Info("Redirect client - Client Number: " + clientNumber + " - ID: " + peer.ID + ", " + serverResponse.Address + ":" + serverResponse.Port);
			++clientNumber;
		}

		static void ExportStatistics(Host server)
		{
			using (var stream = File.Open(csvPath, FileMode.Append))
			using (var writer = new StreamWriter(stream))
			using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
			{
				// write header before first line
				if (exportFirstLine)
				{
					csv.WriteField("Time");
					csv.WriteHeader<Host>();
					csv.NextRecord();
					exportFirstLine = false;
				}
				// write statistics data
				csv.WriteField(DateTime.Now);
				csv.WriteRecord(server);
				csv.NextRecord();
			}

		}


		static void Main(string[] args)
		{
			Library.Initialize();

			csvPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "exports/", DateTime.Now.ToString("yyyyMMdd_HHmmssfffff") + "_statistics.csv");
			Directory.CreateDirectory(Path.GetDirectoryName(csvPath));

			var prevTime = Library.Time;
			var elapsedTimeStatExport = 0f;
			using (Host server = new Host())
			{
				Address address = new Address();

				address.Port = Constants.ENTRY_SERVER_PORT;
				server.Create(address, (int)Library.maxPeers);

				Event netEvent;

				while (!Console.KeyAvailable)
				{
					var currTime = Library.Time;
					elapsedTimeStatExport += currTime - prevTime;
					prevTime = currTime;

					bool polled = false;

					while (!polled)
					{
						if (server.CheckEvents(out netEvent) <= 0)
						{
							if (server.Service(15, out netEvent) <= 0)
								break;

							polled = true;
						}

						switch (netEvent.Type)
						{
							case EventType.None:
								break;

							case EventType.Connect:
								Logger.Info("Client connected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								RedirectClient(netEvent.Peer);
								break;

							case EventType.Disconnect:
								Logger.Info("Client disconnected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								break;

							case EventType.Timeout:
								Logger.Info("Client timeout - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								break;

							case EventType.Receive:
								Logger.Debug("Packet received from - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP + ", Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length);

								byte[] buffer = new byte[netEvent.Packet.Length];
								netEvent.Packet.CopyTo(buffer);

								var serverMessage = ServerMessage.Deserialize(buffer);
								Logger.Trace("Packet data: " + serverMessage.ToString());

								netEvent.Packet.Dispose();

								break;
						}
					}

					while (elapsedTimeStatExport >= Constants.MS_PER_STATISTICS_EXPORT)
					{
						ExportStatistics(server);
						elapsedTimeStatExport -= Constants.MS_PER_STATISTICS_EXPORT;
					}
				}

				Logger.Info("");
				Logger.Info("");
				Logger.Info("################################################");
				Logger.Info("################## STATISTICS ##################");
				Logger.Info("################################################");
				Logger.Info("Packets sent: " + server.PacketsSent);
				Logger.Info("Packets received: " + server.PacketsReceived);
				Logger.Info("Bytes sent: " + server.BytesSent);
				Logger.Info("Bytes received: " + server.BytesReceived);
				Logger.Info("Peers: " + server.PeersCount);

				server.Flush();
			}

			Library.Deinitialize();
		}
	}
}
