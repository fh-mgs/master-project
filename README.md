# IMPORTANT
- when changing client amount also change NUMBER_CLIENTS in Common.Constants
- when changing server amount also change NUMBER_SERVERS in Common.Constants

# Commands
- EntryServer.exe
- Server.exe ID Neighbor_Right_ID Neighbor_Top_ID Neighbor_Left_ID Neighbor_Bottom_ID
  ID ............... ushort, unique server id, decides port (5001 + ID)
  Neighbor_X_ID .... ushort or -1 when no neighbor, ID of neighbor zone server (right, top, left, bottom)
- Client.exe

# Assumptions
- map size: 500 x 500
- players move random within map boundaries (but not necessarily withing server boundaries)
- players move in random direction, changing random rotation every 3 seconds
- players send updates 60 times per second
- clients broadcast game state 20 times per second
- zoning servers are ordered like a grid:
	21 22 23 24 25
	16 17 18 19 20
	11 12 13 14 15
	 6  7  8  9 10
	 1  2  3  4  5
- number of zoning servers is sqrt-able to ensure whole grid
- clients do not exit
- client data is always correct (clients do not use updated server player state to save client performance)

# TODO
- disable logging in release

# Ideas
- maybe test for unequally distributed player number among servers?
- can also make tests where players move between servers
- measure RAM, CPU, ... (load testing tool?)
- also simulate enemies / NPCs - will make difference (f.e. with replication every server needs to simulate all enemies, with zoning only enemies in corresponding zone)