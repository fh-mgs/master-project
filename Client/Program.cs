﻿using System;
using Common;
using Common.Classes;
using ENet;

namespace Client
{
	enum ClientStatus
    {
		Active,
		ConnectedToEntryServer,
		RedirectInitiated,
		ConnectedToServer,
    }

    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            Library.Initialize();

            Client client = new Client(Constants.SERVER_HOST, Constants.ENTRY_SERVER_PORT);
            client.Run();

			Library.Deinitialize();
        }
    }
}
