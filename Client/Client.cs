﻿using Common;
using Common.Classes;
using CsvHelper;
using ENet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Numerics;
using System.Text;

namespace Client
{
    public class Client
    {
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		private Address entryServerAddress;
        private Address serverAddress; // distributed server
        private ClientStatus clientStatus = ClientStatus.Active;
		private EntityState playerState;
		private uint updateCount = 0;
		private string csvPath;
		private bool exportFirstLine = true;

		public Client(string entryServerHost, ushort entryServerPort)
        {
            entryServerAddress = new Address();
            entryServerAddress.SetHost(entryServerHost);
            entryServerAddress.Port = entryServerPort;

            serverAddress = new Address();
			playerState = new EntityState();
			csvPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "exports/", DateTime.Now.ToString("yyyyMMdd_HHmmssfffff") + "_statistics.csv");
			Directory.CreateDirectory(Path.GetDirectoryName(csvPath));
		}

		void ExportStatistics(Host client)
        {
			using (var stream = File.Open(csvPath, FileMode.Append))
			using (var writer = new StreamWriter(stream))
			using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
			{
				// write header before first line
				if (exportFirstLine)
				{
					csv.WriteField("Time");
					csv.WriteField("ClientId");
					csv.WriteHeader<Host>();
					csv.NextRecord();
					exportFirstLine = false;
				}
				// write statistics data
				csv.WriteField(DateTime.Now);
				csv.WriteField(playerState.ClientID);
				csv.WriteRecord(client);
				csv.NextRecord();
			}

		}

		void HandleConnect()
        {
			if (clientStatus == ClientStatus.Active)
			{
				Logger.Info("Client connected to entry server");
				clientStatus = ClientStatus.ConnectedToEntryServer;
			}
			else if (clientStatus == ClientStatus.RedirectInitiated)
			{
				Logger.Info("Client connected to server");
				clientStatus = ClientStatus.ConnectedToServer;
			}
			else
			{
				Logger.Error("Client status incorrect - Connect - " + clientStatus);
			}
		}

        void HandleDisconnect(ref Peer peer, Host client)
        {
			if (clientStatus == ClientStatus.RedirectInitiated)
            {
				Logger.Info("Client disconnected from server during redirect");
				peer = client.Connect(serverAddress, 1, playerState.ClientID.Value);
			}
			else if (clientStatus == ClientStatus.ConnectedToEntryServer)
            {
				Logger.Info("Client disconnected from entry server");
				clientStatus = ClientStatus.Active;
			}
			else if (clientStatus == ClientStatus.ConnectedToServer)
            {
				Logger.Info("Client disconnected from server");
				playerState.ClientID = null;
				clientStatus = ClientStatus.Active;
			}
			else
            {
				Logger.Error("Client status incorrect - Disconnect - " + clientStatus);
			}
		}

		void Update(Peer peer)
        {
			++updateCount;

			// do nothing if not connected to server
			if (playerState.ClientID == null || clientStatus != ClientStatus.ConnectedToServer) return;

			// change random direction if needed
			if (updateCount % Constants.CHANGE_MOVE_ROTATION_AFTER_UPDATES == 0) 
				playerState.Rotation = Vector3.Normalize(Methods.GetRandomVector());
			// move player in direction, ensure that position is in map bounds
			playerState.Position += playerState.Rotation * Constants.MOVE_SPEED * Constants.MS_PER_UPDATE / 1000f;
			playerState.Position = Vector3.Clamp(playerState.Position, Vector3.Zero, new Vector3(Constants.MAP_SIZE));

			// send new player state to server
			Packet packet = default(Packet);
			byte[] data = new ServerMessage() { 
				MessageType = MessageType.PlayerMove, 
				Data = new PlayerMoveCommand() { CommandTime = Library.Time, Direction = playerState.Rotation } 
			}.Serialize();

			packet.Create(data);
			peer.Send(0, ref packet);
		}

		public void Run()
        {
			var prevTime = Library.Time;
			var elapsedTime = 0f;
			var elapsedTimeStatExport = 0f;
			using (Host client = new Host())
            {
                client.Create();

				// initially connect to entry server
				Peer peer = client.Connect(entryServerAddress);

				Event netEvent;

				while (!Console.KeyAvailable)
				{
					var currTime = Library.Time;
					elapsedTime += currTime - prevTime;
					elapsedTimeStatExport += currTime - prevTime;
					prevTime = currTime;

					bool polled = false;

					while (!polled)
					{
						if (client.CheckEvents(out netEvent) <= 0)
						{
							if (client.Service(15, out netEvent) <= 0)
								break;

							polled = true;
						}

						switch (netEvent.Type)
						{
							case EventType.None:
								break;

							case EventType.Connect:
								HandleConnect();
								break;

							case EventType.Disconnect:
								HandleDisconnect(ref peer, client);
								break;

							case EventType.Timeout:
								ExportStatistics(client);

								Logger.Info("Client connection timeout");
								Logger.Info("");
								Logger.Info("");
								Logger.Info("################################################");
								Logger.Info("################## STATISTICS ##################");
								Logger.Info("################################################");
								Logger.Info("Packets sent: " + client.PacketsSent);
								Logger.Info("Packets received: " + client.PacketsReceived);
								Logger.Info("Bytes sent: " + client.BytesSent);
								Logger.Info("Bytes received: " + client.BytesReceived);
								Logger.Info("Peers: " + client.PeersCount);
								break;

							case EventType.Receive:
								Logger.Debug("Packet received from server - Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length);
								byte[] buffer = new byte[netEvent.Packet.Length];
								netEvent.Packet.CopyTo(buffer);

								//Logger.Debug("Packet received from - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP + ", Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length + ", Data: " + Encoding.UTF8.GetString(buffer));

								netEvent.Packet.Dispose();
								var serverMessage = ServerMessage.Deserialize(buffer);

								Logger.Trace("Packet data: " + serverMessage.ToString());

								// server message contains client id and redirect data
								if (serverMessage.MessageType == MessageType.EntryServerRedirect)
                                {
									clientStatus = ClientStatus.RedirectInitiated;
									var data = (EntryServerRedirectData)serverMessage.Data;
									playerState.ClientID = data.ClientID;
									serverAddress.SetHost(data.Address);
									serverAddress.Port = data.Port;
									peer.Disconnect(0);
                                }
								// server message contains redirect data
								if (serverMessage.MessageType == MessageType.ServerRedirect)
								{
									clientStatus = ClientStatus.RedirectInitiated;
									var data = (ServerRedirectData)serverMessage.Data;
									serverAddress.SetHost(data.Address);
									serverAddress.Port = data.Port;
									peer.Disconnect(0);
								}
								// get initial player state
								else if (serverMessage.MessageType == MessageType.PlayerState)
                                {
									playerState = (EntityState)serverMessage.Data;
								}
								else if (serverMessage.MessageType == MessageType.PlayerLogin)
                                {
									// Handle other player logging in
									// not done to save simulated client performance
                                }
								else if (serverMessage.MessageType == MessageType.PlayerLogout)
                                {
									// Handle other player logging out
									// not done to save simulated client performance
								}
								else if (serverMessage.MessageType == MessageType.GameState)
                                {
									// Here the client would receive all player states from the server and interpolate by the received server calculation timestamp and player state values
									// not done to save simulated client performance
								}

								break;
						}
					}

					while (elapsedTime >= Constants.MS_PER_UPDATE)
                    {
						Update(peer);
						elapsedTime -= Constants.MS_PER_UPDATE;
					}

					while (elapsedTimeStatExport >= Constants.MS_PER_STATISTICS_EXPORT)
					{
						ExportStatistics(client);
						elapsedTimeStatExport -= Constants.MS_PER_STATISTICS_EXPORT;
					}

					// Render(elapsedTime);
				}

				ExportStatistics(client);

				Logger.Info("");
				Logger.Info("");
				Logger.Info("################################################");
				Logger.Info("################## STATISTICS ##################");
				Logger.Info("################################################");
				Logger.Info("Packets sent: " + client.PacketsSent);
				Logger.Info("Packets received: " + client.PacketsReceived);
				Logger.Info("Bytes sent: " + client.BytesSent);
				Logger.Info("Bytes received: " + client.BytesReceived);
				Logger.Info("Peers: " + client.PeersCount);

				client.Flush();
			}
		}
    }
}
