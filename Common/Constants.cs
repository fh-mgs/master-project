﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class Constants
    {
        public const float MAP_SIZE = 200f; // units
        public const float MS_PER_UPDATE = 1000f / 60f; // 60fps, ~16.67
        public const float MS_PER_PLAYERSTATE_BROADCAST = 50f; // 20 times per second, depending on how accurate player positions need to be for the clients
        public const float MS_PER_STATISTICS_EXPORT = 1000f; // export statistics every 1 seconds
        public const float MOVE_SPEED = 1.5f; // units per second
        public const uint CHANGE_MOVE_ROTATION_AFTER_UPDATES = 1800; // players change rotation after 30 seconds
        public const uint CHANGE_AI_MOVE_ROTATION_AFTER_UPDATES = 180; // players change rotation after 3 seconds
        public const uint NUMBER_CLIENTS = 20;
        public const uint NUMBER_SERVERS = 4;
        //public const float SERVER_SWAP_TIME_SECONDS = 2f; // players move between servers every 2 seconds (zoning)
        public const ushort ENTRY_SERVER_PORT = 5000;
        public const ushort SERVER_PORT_START = 5001;
        public const string SERVER_HOST = "127.0.0.1";
        public const uint STOP_SERVER_AFTER_MILLISECONDS = 60000; // server stops after x milliseconds and shows stats, 0 to disable stopping
        public const uint NUMBER_ENEMIES = 100; // 1 enemy per 20x20 units
    }
}
