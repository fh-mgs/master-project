﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;

namespace Common.Classes
{
    public class EntityState : IServerMessageData
    {
        public uint? ClientID { get; set; }
        public uint CommandTime { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }

        public void ReadFromBinaryReader(BinaryReader reader)
        {
            ClientID = reader.ReadUInt32();
            CommandTime = reader.ReadUInt32();
            Position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
            Rotation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }

        public void WriteToBinaryWriter(BinaryWriter writer)
        {
            writer.Write(ClientID.Value);
            writer.Write(CommandTime);
            writer.Write(Position.X);
            writer.Write(Position.Y);
            writer.Write(Position.Z);
            writer.Write(Rotation.X);
            writer.Write(Rotation.Y);
            writer.Write(Rotation.Z);
        }
    }
}
