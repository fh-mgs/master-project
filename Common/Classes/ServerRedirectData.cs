﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Classes
{
    // used to redirect client to responsible game server
    public class ServerRedirectData : IServerMessageData
    {
        public string Address { get; set; }
        public ushort Port { get; set; }

        public void ReadFromBinaryReader(BinaryReader reader)
        {
            Address = reader.ReadString();
            Port = reader.ReadUInt16();
        }

        public void WriteToBinaryWriter(BinaryWriter writer)
        {
            writer.Write(Address);
            writer.Write(Port);
        }
    }
}
