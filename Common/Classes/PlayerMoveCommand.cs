﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;

namespace Common.Classes
{
    public class PlayerMoveCommand : IServerMessageData
    {
        public uint CommandTime { get; set; }
        public Vector3 Direction { get; set; }

        public void ReadFromBinaryReader(BinaryReader reader)
        {
            CommandTime = reader.ReadUInt32();
            Direction = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }

        public void WriteToBinaryWriter(BinaryWriter writer)
        {
            writer.Write(CommandTime);
            writer.Write(Direction.X);
            writer.Write(Direction.Y);
            writer.Write(Direction.Z);
        }
    }
}
