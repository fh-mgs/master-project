﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Classes
{
    public enum MessageType : byte
    {
        ServerRedirect,
        EntryServerRedirect,
        PlayerState,
        PlayerLogin,
        PlayerLogout,
        GameState,
        PlayerMove
    }

    public class ServerMessage
    {
        public MessageType MessageType { get; set; }
        public IServerMessageData Data { get; set; }

#if REPLICATION
        public byte[] SerializeActiveGameState()
        {
            using (MemoryStream m = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(m))
                {
                    writer.Write((byte)MessageType);
                    ((GameState)Data).WriteActiveToBinaryWriter(writer);
                }
                return m.ToArray();
            }
        }
#endif

        public byte[] Serialize()
        {
            using (MemoryStream m = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(m))
                {
                    writer.Write((byte)MessageType);
                    Data.WriteToBinaryWriter(writer);
                }
                return m.ToArray();
            }
        }

        public static ServerMessage Deserialize(byte[] data)
        {
            ServerMessage result = new ServerMessage();
            using (MemoryStream m = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(m))
                {
                    result.MessageType = (MessageType)reader.ReadByte();

                    switch (result.MessageType)
                    {
                        case MessageType.ServerRedirect:
                            result.Data = new ServerRedirectData();
                            break;
                        case MessageType.EntryServerRedirect:
                            result.Data = new EntryServerRedirectData();
                            break;
                        case MessageType.PlayerState:
                            result.Data = new EntityState();
                            break;
                        case MessageType.PlayerLogin:
                            result.Data = new ClientID();
                            break;
                        case MessageType.PlayerLogout:
                            result.Data = new ClientID();
                            break;
                        case MessageType.GameState:
                            result.Data = new GameState();
                            break;
                        case MessageType.PlayerMove:
                            result.Data = new PlayerMoveCommand();
                            break;
                        default:
                            break;
                    }

                    result.Data.ReadFromBinaryReader(reader);
                }
            }
            return result;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
