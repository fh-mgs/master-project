﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Classes
{
    public class ClientID : IServerMessageData
    {
        public uint ID;

        public void ReadFromBinaryReader(BinaryReader reader)
        {
            ID = reader.ReadUInt32();
        }

        public void WriteToBinaryWriter(BinaryWriter writer)
        {
            writer.Write(ID);
        }
    }
}
