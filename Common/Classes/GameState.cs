﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Classes
{
    public class GameState : IServerMessageData
    {
        public IDictionary<uint, EntityState> ActiveEntities { get; set; } = new Dictionary<uint, EntityState>(); // <player id, player state>
#if REPLICATION
        public IDictionary<ushort, IDictionary<uint, EntityState>> ShadowEntities { get; set; } = new Dictionary<ushort, IDictionary<uint, EntityState>>(); // <server port, <player id, player state>>

        public void WriteActiveToBinaryWriter(BinaryWriter writer)
        {
            foreach (var playerState in ActiveEntities)
            {
                playerState.Value.WriteToBinaryWriter(writer);
            }
        }
#endif

        public void ReadFromBinaryReader(BinaryReader reader)
        {
            while (reader.BaseStream.Position != reader.BaseStream.Length)
            {
                var playerState = new EntityState();
                playerState.ReadFromBinaryReader(reader);
                ActiveEntities[playerState.ClientID.Value] = playerState;
            }
        }

        public void WriteToBinaryWriter(BinaryWriter writer)
        {
            foreach (var playerState in ActiveEntities)
            {
                playerState.Value.WriteToBinaryWriter(writer);
            }
#if REPLICATION
            foreach (var playerState in ShadowEntities.SelectMany(s => s.Value))
            {
                playerState.Value.WriteToBinaryWriter(writer);
            }
#endif
        }

    }
}
