﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Interfaces
{
    public  interface IServerMessageData
    {
        void WriteToBinaryWriter(BinaryWriter writer);
        void ReadFromBinaryReader(BinaryReader reader);
    }
}
