﻿using System;
using System.Numerics;
using System.Text;
using Common.Classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Common
{
    public static class Methods
    {
        static Random random = new Random();

        //public static byte[] SerializeObject(object value) => Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value));

        //public static ServerMessage DeserializeServerMessage(byte[] value)
        //{
        //    var serverMessage = JsonConvert.DeserializeObject<ServerMessage>(Encoding.UTF8.GetString(value));
        //    switch (serverMessage.MessageType)
        //    {
        //        case MessageType.ServerRedirect:
        //            serverMessage.Data = ((JObject)serverMessage.Data).ToObject<ServerRedirectData>();
        //            return serverMessage;
        //        case MessageType.PlayerState:
        //            serverMessage.Data = ((JObject)serverMessage.Data).ToObject<PlayerState>();
        //            return serverMessage;
        //        default:
        //            return serverMessage;
        //    }
        //}

        public static float GetRandomFloat() => (float)random.NextDouble();
        public static float GetRandomFloat(float minInclusive, float maxExclusive) => (float)(random.NextDouble() * (maxExclusive - minInclusive) + minInclusive);

        public static Vector3 GetRandomVector(float minInclusive, float maxExclusive) =>
            new Vector3(
                GetRandomFloat(minInclusive, maxExclusive),
                0f, //GetRandomFloat(minInclusive, maxExclusive),
                GetRandomFloat(minInclusive, maxExclusive)
            );

        public static Vector3 GetRandomVector(Vector3 minInclusive, Vector3 maxExclusive) =>
            new Vector3(
                GetRandomFloat(minInclusive.X, maxExclusive.X),
                GetRandomFloat(minInclusive.Y, maxExclusive.Y),
                GetRandomFloat(minInclusive.Z, maxExclusive.Z)
            );

        public static Vector3 GetRandomVector() =>
            new Vector3(
                GetRandomFloat() * 2 - 1, // -1 to 1
                0f, //GetRandomFloat(),
                GetRandomFloat() * 2 - 1 // -1 to 1
            );

    }
}
