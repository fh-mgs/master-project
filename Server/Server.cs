﻿using Common;
using Common.Classes;
using CsvHelper;
using CsvHelper.Configuration;
using ENet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Server
{
	public class Server
	{
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		ushort serverId;
		string host;
		ushort port;
		Vector3 serverMapStartPos;
		Vector3 serverMapSize;
		Random random = new Random();
		GameState gameState = new GameState();
		Dictionary<uint, uint> peerPlayerMap = new Dictionary<uint, uint>(); // <peer id, player id>
		uint numberEnemies;
		uint aiUpdateCount = 0;
		Statistics statistics;
		string csvPath;
		bool useGUI;
        SFML.Graphics.RenderWindow window;

		class Statistics
		{
			private bool exportFirstLine = true;
			private string csvPath;

			public DateTime Time => DateTime.Now;
			public ushort ServerId { get; } = 0;

			public Statistics(ushort serverId, string csvPath)
            {
				ServerId = serverId;
				this.csvPath = csvPath;

				Directory.CreateDirectory(Path.GetDirectoryName(csvPath));
            }

			// enet statistics
			public uint PacketsSent { get; private set; } = 0;
			public uint PacketsReceived { get; private set; } = 0;
			public uint BytesSent { get; private set; } = 0;
			public uint BytesReceived { get; private set; } = 0;
			public uint PeersCount { get; private set; } = 0;

			// custom statistics
			public uint PeerConnects { get; set; } = 0;
			public uint PeerConnectsPacketsSent { get; set; } = 0;
			public uint PeerConnectsBytesSent { get; set; } = 0;
			public uint PeerRedirects { get; set; } = 0;
			public uint PeerRedirectsPacketsSent { get; set; } = 0;
			public uint PeerRedirectsBytesSent { get; set; } = 0;
			public uint PeerDisconnects { get; set; } = 0;
			public uint PeerDisconnectsPacketsSent { get; set; } = 0;
			public uint PeerDisconnectsBytesSent { get; set; } = 0;
			public uint ServerCommunicationPacketsSent { get; set; } = 0;
			public uint ServerCommunicationPacketsReceived { get; set; } = 0;
			public uint ServerCommunicationBytesSent { get; set; } = 0;
			public uint ServerPlayerStateUpdateBytesSent { get; set; } = 0;
			public uint ServerEnemyUpdateBytesSent { get; set; } = 0;
			public uint ServerCommunicationBytesReceived { get; set; } = 0;
			public uint ClientCommunicationPacketsSent { get; set; } = 0;
			public uint ClientCommunicationPacketsReceived { get; set; } = 0;
			public uint ClientCommunicationBytesSent { get; set; } = 0;
			public uint ClientPlayerStateUpdateBytesSent { get; set; } = 0;
			public uint ClientEnemyUpdateBytesSent { get; set; } = 0;
			public uint ClientCommunicationBytesReceived { get; set; } = 0;

			public void ExportStatistics(Host server)
            {
				// set enet statistics
				PacketsSent = server.PacketsSent;
				PacketsReceived = server.PacketsReceived;
				BytesSent = server.BytesSent;
				BytesReceived = server.BytesReceived;
				PeersCount = server.PeersCount;

				using (var stream = File.Open(csvPath, FileMode.Append))
				using (var writer = new StreamWriter(stream))
				using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
				{
					// write header before first line
					if (exportFirstLine)
                    {
						csv.WriteHeader<Statistics>();
						csv.NextRecord();
						exportFirstLine = false;
					}
					// write statistics data
					csv.WriteRecord(this);
					csv.NextRecord();
				}
			}
			public void LogStatistics(NLog.LogLevel logLevel, Host server)
			{
				Logger.Log(logLevel, "");
				Logger.Log(logLevel, "");
				Logger.Log(logLevel, "################################################");
				Logger.Log(logLevel, "################## STATISTICS ##################");
				Logger.Log(logLevel, "################################################");
				// enet statistics
				Logger.Log(logLevel, "Packets sent: " + server.PacketsSent);
				Logger.Log(logLevel, "Packets received: " + server.PacketsReceived);
				Logger.Log(logLevel, "Bytes sent: " + server.BytesSent);
				Logger.Log(logLevel, "Bytes received: " + server.BytesReceived);
				Logger.Log(logLevel, "Peers: " + server.PeersCount);
				// custom statistics
				Logger.Log(logLevel, "PeerConnects: " + PeerConnects);
				Logger.Log(logLevel, "PeerConnectsPacketsSent: " + PeerConnectsPacketsSent);
				Logger.Log(logLevel, "PeerConnectsBytesSent: " + PeerConnectsBytesSent);
				Logger.Log(logLevel, "PeerRedirects: " + PeerRedirects);
				Logger.Log(logLevel, "PeerRedirectsPacketsSent: " + PeerRedirectsPacketsSent);
				Logger.Log(logLevel, "PeerRedirectsBytesSent: " + PeerRedirectsBytesSent);
				Logger.Log(logLevel, "PeerDisconnects: " + PeerDisconnects);
				Logger.Log(logLevel, "PeerDisconnectsPacketsSent: " + PeerDisconnectsPacketsSent);
				Logger.Log(logLevel, "PeerDisconnectsBytesSent: " + PeerDisconnectsBytesSent);
				Logger.Log(logLevel, "ServerCommunicationPacketsSent: " + ServerCommunicationPacketsSent);
				Logger.Log(logLevel, "ServerCommunicationPacketsReceived: " + ServerCommunicationPacketsReceived);
				Logger.Log(logLevel, "ServerCommunicationBytesSent: " + ServerCommunicationBytesSent);
				Logger.Log(logLevel, "ServerPlayerStateUpdateBytesSent: " + ServerPlayerStateUpdateBytesSent);
				Logger.Log(logLevel, "ServerEnemyUpdateBytesSent: " + ServerEnemyUpdateBytesSent);
				Logger.Log(logLevel, "ServerCommunicationBytesReceived: " + ServerCommunicationBytesReceived);
				Logger.Log(logLevel, "ClientCommunicationPacketsSent: " + ClientCommunicationPacketsSent);
				Logger.Log(logLevel, "ClientCommunicationPacketsReceived: " + ClientCommunicationPacketsReceived);
				Logger.Log(logLevel, "ClientCommunicationBytesSent: " + ClientCommunicationBytesSent);
				Logger.Log(logLevel, "ClientPlayerStateUpdateBytesSent: " + ClientPlayerStateUpdateBytesSent);
				Logger.Log(logLevel, "ClientEnemyUpdateBytesSent: " + ClientEnemyUpdateBytesSent);
				Logger.Log(logLevel, "ClientCommunicationBytesReceived: " + ClientCommunicationBytesReceived);
			}
		}

#if ZONING
		ushort? neighborZonePortRight;
		ushort? neighborZonePortTop;
		ushort? neighborZonePortLeft;
		ushort? neighborZonePortBottom;

		IList<Peer> clients = new List<Peer>();
		IList<Peer> servers = new List<Peer>();

		IEnumerable<ushort> otherServerPorts;
#elif REPLICATION
		GameState shadowGameState = new GameState();
		IList<Peer> clients = new List<Peer>();
		IList<Peer> servers = new List<Peer>();

		IEnumerable<ushort> otherServerPorts;
#endif

#if REPLICATION
		public Server(ushort serverId, sstring serverHost, ushort serverPort, bool useGUI, Vector3 serverMapStartPos, Vector3 serverMapSize, IEnumerable<ushort> otherServerPorts)
		{
			this.otherServerPorts = otherServerPorts;
#elif ZONING
		public Server(ushort serverId, string serverHost, ushort serverPort, bool useGUI, Vector3 serverMapStartPos, Vector3 serverMapSize, ushort? neighborZonePortRight, ushort? neighborZonePortTop, ushort? neighborZonePortLeft, ushort? neighborZonePortBottom)
		{
			this.neighborZonePortRight = neighborZonePortRight;
			this.neighborZonePortTop = neighborZonePortTop;
			this.neighborZonePortLeft = neighborZonePortLeft;
			this.neighborZonePortBottom = neighborZonePortBottom;

			// save not null ports in otherServerPorts
			otherServerPorts = new[] { neighborZonePortRight, neighborZonePortTop, neighborZonePortLeft, neighborZonePortBottom }.Where(p => p.HasValue).Select(p => p.Value);
#else
		public Server(ushort serverId, string serverHost, ushort serverPort, bool useGUI, Vector3 serverMapStartPos, Vector3 serverMapSize)
		{
#endif
			this.serverId = serverId;
			host = serverHost;
			port = serverPort;
			this.serverMapStartPos = serverMapStartPos;
			this.serverMapSize = serverMapSize;

			csvPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "exports/", serverId + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfffff") + "_statistics.csv");
			statistics = new Statistics(serverId, csvPath);

			this.useGUI = useGUI;
			if (useGUI)
            {
				window = new SFML.Graphics.RenderWindow(new SFML.Window.VideoMode(480, 480), "Server " + serverId);
				window.SetView(new SFML.Graphics.View(new SFML.Graphics.FloatRect(0, window.Size.Y - Constants.MAP_SIZE, Constants.MAP_SIZE, Constants.MAP_SIZE)));
				window.Closed += (_, __) => window.Close();
			}

			// add initial enemy positions and rotations
#if INSTANCING
			numberEnemies = Constants.NUMBER_ENEMIES;
#else
			numberEnemies = Constants.NUMBER_ENEMIES / Constants.NUMBER_SERVERS;
#endif
			for (uint i = Constants.NUMBER_CLIENTS; i < Constants.NUMBER_CLIENTS + numberEnemies; i++)
            {
				gameState.ActiveEntities.Add(i, new EntityState()
				{
					ClientID = i,
					Position = serverMapStartPos + Methods.GetRandomVector(Vector3.Zero, serverMapSize), // random position in map
					Rotation = Vector3.Normalize(Methods.GetRandomVector()) // random normalized direction vector
				});
			}
		}

#if ZONING
		void RedirectClient(Peer peer, ushort port)
		{
			// client redirect message
			ServerRedirectData redirectResponse = new ServerRedirectData() { Address = Constants.SERVER_HOST, Port = port };
			ServerMessage redirectMessage = new ServerMessage() { MessageType = MessageType.ServerRedirect, Data = redirectResponse };

			Packet clientPacket = default(Packet);
			byte[] clientData = redirectMessage.Serialize();

			clientPacket.Create(clientData);
			peer.Send(0, ref clientPacket);

			// server initial position message
			EntityState entityState = gameState.ActiveEntities[peerPlayerMap[peer.ID]];
			ServerMessage serverMessage = new ServerMessage() { MessageType = MessageType.PlayerState, Data = entityState };

			Packet serverPacket = default(Packet);
			byte[] serverData = serverMessage.Serialize();

			serverPacket.Create(serverData, PacketFlags.Reliable);
			servers.First(p => p.Port == port).Send(1, ref serverPacket);

			// statistics updates
			++statistics.PeerRedirects;
			++statistics.PeerRedirectsPacketsSent;
			++statistics.ServerCommunicationPacketsSent;
			statistics.PeerRedirectsBytesSent += (uint)clientData.Length;
			statistics.ServerCommunicationBytesSent += (uint)serverData.Length;

			Logger.Info("Redirect client - ID: " + peer.ID + ", " + redirectResponse.Address + ":" + redirectResponse.Port);
			Logger.Debug("Send player state to server: " + serverMessage.ToString());
		}

		bool RedirectIfOutOfServerBounds(Peer peer, Vector3 receivedPlayerPos)
		{
			// check if server redirect required
			if (receivedPlayerPos.X < serverMapStartPos.X && neighborZonePortLeft.HasValue)
			{
				RedirectClient(peer, neighborZonePortLeft.Value);
			}
			else if (receivedPlayerPos.X > serverMapStartPos.X + serverMapSize.X && neighborZonePortRight.HasValue)
			{
				RedirectClient(peer, neighborZonePortRight.Value);
			}
			else if (receivedPlayerPos.Z < serverMapStartPos.Z && neighborZonePortBottom.HasValue)
			{
				RedirectClient(peer, neighborZonePortBottom.Value);
			}
			else if (receivedPlayerPos.Z > serverMapStartPos.Z + serverMapSize.Z && neighborZonePortTop.HasValue)
			{
				RedirectClient(peer, neighborZonePortTop.Value);
			}
			else
			{
				return false;
			}

			return true;
		}
#endif

		Vector3 ClampToMapBounds(Vector3 pos)
		{
			return Vector3.Clamp(pos, Vector3.Zero, new Vector3(Constants.MAP_SIZE));
		}

		void HandleConnect(Peer peer, Host server, uint playerId)
        {
			++statistics.PeerConnects;

#if REPLICATION || ZONING
			if (otherServerPorts.Contains(peer.Port))
            {
				Logger.Info("Server connected - ID: " + peer.ID + ", IP: " + peer.IP + ", Port: " + peer.Port);
				servers.Add(peer);
				return;
			}
			else
            {
				clients.Add(peer);
            }
#endif

			Logger.Info("Client connected - ID: " + peer.ID + ", IP: " + peer.IP);

			EntityState initialPlayerState;
			// get initial player state if player is not comming from other server
			// this could be a database call in a real mmo implementation
			if (!gameState.ActiveEntities.ContainsKey(playerId))
            {
				initialPlayerState = new EntityState()
				{
					ClientID = playerId,
					Position = serverMapStartPos + Methods.GetRandomVector(Vector3.Zero, serverMapSize), // random position in map
					Rotation = Vector3.Normalize(Methods.GetRandomVector()) // random normalized direction vector
				};
				gameState.ActiveEntities.Add(playerId, initialPlayerState);
			}
			else
            {
				initialPlayerState = gameState.ActiveEntities[playerId];
			}
			peerPlayerMap.Add(peer.ID, playerId);

			Packet packet = default(Packet);
			byte[] data = new ServerMessage() { MessageType = MessageType.PlayerState, Data = initialPlayerState }.Serialize();
			packet.Create(data);
			peer.Send(0, ref packet);
			++statistics.PeerConnectsPacketsSent;
			statistics.PeerConnectsBytesSent += (uint)data.Length;

			// broadcast login event
			Packet broadcastPacket = default(Packet);
			byte[] broadcastData = new ServerMessage() { MessageType = MessageType.PlayerLogin, Data = new ClientID() { ID = playerId } }.Serialize();
			broadcastPacket.Create(broadcastData);
#if REPLICATION || ZONING
			server.Broadcast(0, ref broadcastPacket, clients.ToArray());
			statistics.PeerConnectsPacketsSent += (uint)clients.Count;
			statistics.PeerConnectsBytesSent += (uint)(broadcastData.Length * clients.Count);
#else
			server.Broadcast(0, ref broadcastPacket);
			statistics.PeerConnectsPacketsSent += (uint)server.PeersCount;
			statistics.PeerConnectsBytesSent += (uint)(broadcastData.Length * server.PeersCount);
#endif

			Logger.Debug("Send initial player state - ID: " + initialPlayerState.ClientID + ", position: " + initialPlayerState.Position + ", rotation: " + initialPlayerState.Rotation);
		}

		void HandleDisconnect(Host server, uint peerId)
        {
			++statistics.PeerDisconnects;

			var playerId = peerPlayerMap[peerId];
			peerPlayerMap.Remove(peerId);
			gameState.ActiveEntities.Remove(playerId);

			// broadcast logout event
			Packet broadcastPacket = default(Packet);
			byte[] broadcastData = new ServerMessage() { MessageType = MessageType.PlayerLogout, Data = new ClientID() { ID = playerId } }.Serialize();
			broadcastPacket.Create(broadcastData);
#if REPLICATION || ZONING
			server.Broadcast(0, ref broadcastPacket, clients.ToArray());
			statistics.PeerDisconnectsPacketsSent += (uint)clients.Count;
			statistics.PeerDisconnectsBytesSent += (uint)(broadcastData.Length * clients.Count);
#else
			server.Broadcast(0, ref broadcastPacket);
			statistics.PeerDisconnectsPacketsSent += server.PeersCount;
			statistics.PeerDisconnectsBytesSent += (uint)broadcastData.Length * server.PeersCount;
#endif

			Logger.Debug("Broadcast player disconnected - ID: " + peerId);
		}

		void UpdateAIEnemies()
        {
			++aiUpdateCount;

			for (uint i = Constants.NUMBER_CLIENTS; i < Constants.NUMBER_CLIENTS + numberEnemies; i++)
			{
				// change random direction if needed
				if (aiUpdateCount % Constants.CHANGE_MOVE_ROTATION_AFTER_UPDATES == 0)
					gameState.ActiveEntities[i].Rotation = Vector3.Normalize(Methods.GetRandomVector());
				// move player in direction, ensure that position is in map bounds
				gameState.ActiveEntities[i].Position += gameState.ActiveEntities[i].Rotation * Constants.MOVE_SPEED * Constants.MS_PER_UPDATE / 1000f;
				gameState.ActiveEntities[i].Position = Vector3.Clamp(gameState.ActiveEntities[i].Position, serverMapStartPos, serverMapStartPos + serverMapSize);
			}

		}

		void Update(Host server)
        {
			// TODO: broadcast ONLY UPDATED playerstates
#if REPLICATION
			int activePlayerEntities = gameState.ActiveEntities.Where(e => e.Key < Constants.NUMBER_CLIENTS).Count();
			int activeEnemyEntities = gameState.ActiveEntities.Where(e => e.Key >= Constants.NUMBER_CLIENTS).Count();
			int shadowPlayerEntities = gameState.ShadowEntities.SelectMany(p => p.Value).Where(e => e.Key < Constants.NUMBER_CLIENTS).Count();
			int shadowEnemyEntities = gameState.ShadowEntities.SelectMany(p => p.Value).Where(e => e.Key >= Constants.NUMBER_CLIENTS).Count();

			// send server updates
			Packet serverBroadcastPacket = default(Packet);
			byte[] serverBroadcastData = new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.SerializeActiveGameState();
			serverBroadcastPacket.Create(serverBroadcastData);

			Logger.Debug("Broadcast game state to servers - Player IDs: [" + string.Join(",", gameState.ActiveEntities.Where(e => e.Key < Constants.NUMBER_CLIENTS).Select(p => p.Key)) + "], + Enemies: " + activeEnemyEntities + ", Data Length: " + serverBroadcastPacket.Length);
			Logger.Trace("Broadcast game state to servers - data: " + new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.ToString());

			server.Broadcast(0, ref serverBroadcastPacket, servers.ToArray());
			statistics.ServerCommunicationPacketsSent += (uint)servers.Count;
			statistics.ServerCommunicationBytesSent += (uint)(servers.Count * serverBroadcastData.Length);
			statistics.ServerPlayerStateUpdateBytesSent += (uint)(servers.Count * (serverBroadcastData.Length - 1) * activePlayerEntities / (activePlayerEntities + activeEnemyEntities));
			statistics.ServerEnemyUpdateBytesSent += (uint)(servers.Count * (serverBroadcastData.Length - 1) * activeEnemyEntities / (activePlayerEntities + activeEnemyEntities));

			// send client updates
			Packet clientBroadcastPacket = default(Packet);
			byte[] clientBroadcastData = new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.Serialize();
			clientBroadcastPacket.Create(clientBroadcastData);

			Logger.Debug("Broadcast game state to clients - Player IDs: [" + string.Join(",", gameState.ActiveEntities.Concat(gameState.ShadowEntities.SelectMany(p => p.Value)).Where(e => e.Key < Constants.NUMBER_CLIENTS).Select(p => p.Key)) + "], + Enemies: " + (activeEnemyEntities + shadowEnemyEntities) + ", Data Length: " + clientBroadcastPacket.Length);
			Logger.Trace("Broadcast game state to clients - data: " + new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.ToString());

			server.Broadcast(0, ref clientBroadcastPacket, clients.ToArray());
			statistics.ClientCommunicationPacketsSent += (uint)clients.Count;
			statistics.ClientCommunicationBytesSent += (uint)(clients.Count * clientBroadcastData.Length);
            statistics.ClientPlayerStateUpdateBytesSent += (uint)(clients.Count * (clientBroadcastData.Length - 1) * (activePlayerEntities + shadowPlayerEntities) / (activePlayerEntities + activeEnemyEntities + shadowPlayerEntities + shadowEnemyEntities));
            statistics.ClientEnemyUpdateBytesSent += (uint)(clients.Count * (clientBroadcastData.Length - 1) * (activeEnemyEntities + shadowEnemyEntities) / (activePlayerEntities + activeEnemyEntities + shadowPlayerEntities + shadowEnemyEntities));
#else
			int playerEntities = gameState.ActiveEntities.Where(e => e.Key < Constants.NUMBER_CLIENTS).Count();
			int enemyEntities = gameState.ActiveEntities.Where(e => e.Key >= Constants.NUMBER_CLIENTS).Count();

			Packet broadcastPacket = default(Packet);
			byte[] broadcastData = new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.Serialize();
			broadcastPacket.Create(broadcastData);

			Logger.Debug("Broadcast game state - Player IDs: [" + string.Join(",", gameState.ActiveEntities.Where(e => e.Key < Constants.NUMBER_CLIENTS).Select(p => p.Key)) + "], + Enemies: " + enemyEntities + ", Data Length: " + broadcastPacket.Length);
			Logger.Trace("Broadcast game state - data: " + new ServerMessage() { MessageType = MessageType.GameState, Data = gameState }.ToString());

#if ZONING
			server.Broadcast(0, ref broadcastPacket, clients.ToArray());
			statistics.ClientCommunicationPacketsSent += (uint)clients.Count;
			statistics.ClientCommunicationBytesSent += (uint)(clients.Count * broadcastData.Length);
			statistics.ClientPlayerStateUpdateBytesSent += (uint)(clients.Count * (broadcastData.Length - 1) * playerEntities / (playerEntities + enemyEntities));
            statistics.ClientEnemyUpdateBytesSent += (uint)(clients.Count * (broadcastData.Length - 1) * enemyEntities / (playerEntities + enemyEntities));
#else
			server.Broadcast(0, ref broadcastPacket);
			statistics.ClientCommunicationPacketsSent += (uint)server.PeersCount;
			statistics.ClientCommunicationBytesSent += (uint)(server.PeersCount * broadcastData.Length);
			statistics.ClientPlayerStateUpdateBytesSent += (uint)(server.PeersCount * (broadcastData.Length - 1) * playerEntities / (playerEntities + enemyEntities));
			statistics.ClientEnemyUpdateBytesSent += (uint)(server.PeersCount * (broadcastData.Length - 1) * enemyEntities / (playerEntities + enemyEntities));
#endif
#endif
		}

		void Render()
        {
			window.DispatchEvents();
			window.Clear(SFML.Graphics.Color.White);

			foreach (var entity in gameState.ActiveEntities)
            {
				// player
				if (entity.Key < Constants.NUMBER_CLIENTS)
                {
					var shape = new SFML.Graphics.CircleShape(3)
					{
						FillColor = SFML.Graphics.Color.Black,
						Position = new SFML.System.Vector2f(entity.Value.Position.X, window.Size.Y - entity.Value.Position.Z)
					};
					window.Draw(shape);
				}
				// ai enemy
				else
                {
					var shape = new SFML.Graphics.RectangleShape(new SFML.System.Vector2f(3, 3))
					{
						FillColor = SFML.Graphics.Color.Black,
						Position = new SFML.System.Vector2f(entity.Value.Position.X, window.Size.Y - entity.Value.Position.Z)
					};
					window.Draw(shape);

				}
			}

			window.Display();
		}

		public void Run()
        {
			var prevTime = Library.Time;
			var elapsedTimeUpdate = 0f;
			var elapsedTimeAIUpdate = 0f;
			var elapsedTimeStatExport = 0f;
			using (Host server = new Host())
			{
				Address address = new Address();

				address.Port = port;
				server.Create(address, (int)Library.maxPeers, 2);

				Logger.Info("Started server on " + address.GetHost() + ":" + address.Port);

#if REPLICATION || ZONING
				// connect to all other servers
				foreach (var otherserverPort in otherServerPorts)
                {
					// avoid duplicate connections by only connecting to grater port numbers
					if (otherserverPort <= port) continue;

					Address serverAddress = new Address();
					serverAddress.SetHost(Constants.SERVER_HOST);
					serverAddress.Port = otherserverPort;
					server.Connect(serverAddress, 2);
				}
#endif

				Event netEvent;

				while (!Console.KeyAvailable)
				{
					// TODO: start STOP_SERVER_AFTER_MILLISECONDS timer and stat measurement only when all peers connected
					var currTime = Library.Time;
					elapsedTimeUpdate += currTime - prevTime;
					elapsedTimeAIUpdate += currTime - prevTime;
					elapsedTimeStatExport += currTime - prevTime;
					prevTime = currTime;

					bool polled = false;

					while (!polled)
					{
						if (server.CheckEvents(out netEvent) <= 0)
						{
							if (server.Service(15, out netEvent) <= 0)
								break;

							polled = true;
						}

						switch (netEvent.Type)
						{
							case EventType.None:
								break;

							case EventType.Connect:
								HandleConnect(netEvent.Peer, server, netEvent.Data);
								break;

							case EventType.Disconnect:
								Logger.Info("Client disconnected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								HandleDisconnect(server, netEvent.Peer.ID);
								break;

							case EventType.Timeout:
								Logger.Warn("Client timeout - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								HandleDisconnect(server, netEvent.Peer.ID);
								break;

							case EventType.Receive:
								Logger.Debug("Packet received from - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP + ", Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length);

								byte[] buffer = new byte[netEvent.Packet.Length];
								netEvent.Packet.CopyTo(buffer);

								netEvent.Packet.Dispose();

								var serverMessage = ServerMessage.Deserialize(buffer);
#if REPLICATION
								// only accept game state updates from servers
								if (serverMessage.MessageType == MessageType.GameState && otherServerPorts.Contains(netEvent.Peer.Port))
                                {
									var receivedGameState = (GameState)serverMessage.Data;
									// replace server gamestate
									gameState.ShadowEntities[netEvent.Peer.Port] = receivedGameState.ActiveEntities;

									++statistics.ServerCommunicationPacketsReceived;
									statistics.ServerCommunicationBytesReceived += (uint)buffer.Length;
                                }
#endif

								if (serverMessage.MessageType == MessageType.PlayerMove)
								{
									var receivedPlayerMove = (PlayerMoveCommand)serverMessage.Data;
									if (receivedPlayerMove.Direction == Vector3.Zero) receivedPlayerMove.Direction = Vector3.UnitZ;
									var rotation = Vector3.Normalize(receivedPlayerMove.Direction);
									var position = gameState.ActiveEntities[peerPlayerMap[netEvent.Peer.ID]].Position + rotation * Constants.MOVE_SPEED * Constants.MS_PER_UPDATE / 1000f;
									position = ClampToMapBounds(position);

#if ZONING
									if (!RedirectIfOutOfServerBounds(netEvent.Peer, position))
									{
										gameState.ActiveEntities[peerPlayerMap[netEvent.Peer.ID]].Position = position;
									}
#else
									gameState.ActiveEntities[peerPlayerMap[netEvent.Peer.ID]].Position = position;
#endif
									++statistics.ClientCommunicationPacketsReceived;
									statistics.ClientCommunicationBytesReceived += (uint)buffer.Length;
								}
#if ZONING
								// accept initial player state from other servers
								else if (serverMessage.MessageType == MessageType.PlayerState && otherServerPorts.Contains(netEvent.Peer.Port))
                                {
									var receivedPlayerState = (EntityState)serverMessage.Data;
									gameState.ActiveEntities[receivedPlayerState.ClientID.Value] = receivedPlayerState;

									++statistics.ServerCommunicationPacketsReceived;
									statistics.ServerCommunicationBytesReceived += (uint)buffer.Length;

									Logger.Debug("Received player state from server: " + serverMessage.ToString());
								}
#endif

								Logger.Trace("Packet data: " + serverMessage.ToString()); ;

								break;
						}
					}

					while (elapsedTimeAIUpdate >= Constants.MS_PER_UPDATE)
					{
						UpdateAIEnemies();
						elapsedTimeAIUpdate -= Constants.MS_PER_UPDATE;
					}

					while (elapsedTimeUpdate >= Constants.MS_PER_PLAYERSTATE_BROADCAST)
                    {
						Update(server);
						elapsedTimeUpdate -= Constants.MS_PER_PLAYERSTATE_BROADCAST;
                    }

					while (elapsedTimeStatExport >= Constants.MS_PER_STATISTICS_EXPORT)
                    {
						statistics.ExportStatistics(server);
						elapsedTimeStatExport -= Constants.MS_PER_STATISTICS_EXPORT;
                    }

					if (useGUI && window.IsOpen)
                    {
						Render();
                    }

					// stop server if Constants.STOP_SERVER_AFTER_MILLISECONDS set
					if (Constants.STOP_SERVER_AFTER_MILLISECONDS > 0 && currTime >= Constants.STOP_SERVER_AFTER_MILLISECONDS) 
						break;
				}

				statistics.ExportStatistics(server);
				statistics.LogStatistics(NLog.LogLevel.Info, server);

				server.Flush();
			}
		}
	}
}
