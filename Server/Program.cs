﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using Common;
using Common.Classes;
using ENet;

namespace Server
{
    class Program
    {
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		static void Main(string[] args)
        {
			ushort serverId;
			ushort port = Constants.SERVER_PORT_START;
			bool useGUI = false;

			Server server;

			int sqrtNumServers = (int)Math.Sqrt(Constants.NUMBER_SERVERS);

			if (args.Length == 2 && args[1] == "--gui")
			{
				useGUI = true;
			}
			if (args.Length < 1)
			{
				Logger.Error("Missing Arguments: ServerID");
				return;
			}
			else if (!ushort.TryParse(args[0], out serverId))
            {
				Logger.Error($"Argument 0 is not of type ushort.");
				return;
			}
			else
            {
#if ZONING

				port = (ushort)(Constants.SERVER_PORT_START + serverId);

				var row = (int)(serverId / sqrtNumServers);
				var col = (int)(serverId % sqrtNumServers);
				var serverMapStartPos = new Vector3(((float)col / sqrtNumServers * Constants.MAP_SIZE), 0, ((float)row / sqrtNumServers * Constants.MAP_SIZE));
				var serverMapSize = new Vector3((Constants.MAP_SIZE / sqrtNumServers), 0, (Constants.MAP_SIZE / sqrtNumServers));

				ushort? neighborZonePortRight = col + 1 < sqrtNumServers ? (ushort?)Convert.ToUInt16(Constants.SERVER_PORT_START + row * sqrtNumServers + col + 1) : null;
				ushort? neighborZonePortLeft = col - 1 >= 0 ? (ushort?)Convert.ToUInt16(Constants.SERVER_PORT_START + row * sqrtNumServers + col - 1) : null;
				ushort? neighborZonePortTop = row + 1 < sqrtNumServers ? (ushort?)Convert.ToUInt16(Constants.SERVER_PORT_START + (row + 1) * sqrtNumServers + col) : null;
				ushort? neighborZonePortBottom = row - 1 >= 0 ? (ushort?)Convert.ToUInt16(Constants.SERVER_PORT_START + (row - 1) * sqrtNumServers + col) : null;
				
				server = new Server(serverId, Constants.SERVER_HOST, port, useGUI, serverMapStartPos, serverMapSize, neighborZonePortRight, neighborZonePortTop, neighborZonePortLeft, neighborZonePortBottom);

#elif INSTANCING

				port = (ushort)(Constants.SERVER_PORT_START + serverId);
				var serverMapStartPos = Vector3.Zero;
				var serverMapSize = new Vector3(Constants.MAP_SIZE);

				server = new Server(serverId, Constants.SERVER_HOST, port, useGUI, serverMapStartPos, serverMapSize);

#elif REPLICATION

				port = (ushort)(Constants.SERVER_PORT_START + serverId);
				var serverMapStartPos = Vector3.Zero;
				var serverMapSize = new Vector3(Constants.MAP_SIZE);

				List<ushort> otherServerPorts = new List<ushort>();
				for (var i = 0; i < Constants.NUMBER_SERVERS; ++i)
                {
					var nextServer = (ushort)(Constants.SERVER_PORT_START + i);

					// ignore own port
					if (nextServer != port)
						otherServerPorts.Add(nextServer);
                }

				server = new Server(serverId, Constants.SERVER_HOST, port, useGUI, serverMapStartPos, serverMapSize, otherServerPorts);

#endif
			}

			Library.Initialize();

			server.Run();

			Library.Deinitialize();
        }
    }
}
